import java.util.ArrayList;
import java.util.Arrays;

public class Player
{
    private String uniqueID;
    private String pass;
    private int rounds;
    private int wordsFound;
    private int points;

    //no-arg constructor
    public Player()
    {
        this.uniqueID = null;
        this.pass = null;
        this.rounds = 0;
        this.wordsFound = 0;
        this.points = 0;
    }

    //create player passing in exact parts
    public Player(String uniqueID, String password, int rounds, int wordsFound, int points)
    {
        this.uniqueID = uniqueID;
        this.pass = password;
        this.rounds = rounds;
        this.wordsFound = wordsFound;
        this.points = points;
    }

    //create a player from a string containing the necessary parts
    //needed for reading in from player file
    public Player(String player, int importing)
    {
        ArrayList<String> playerParts = new ArrayList<>(Arrays.asList(player.split(",")));

        uniqueID = playerParts.get(0);
        pass = playerParts.get(1);
        rounds = Integer.parseInt(playerParts.get(2));
        wordsFound = Integer.parseInt(playerParts.get(3));
        points = Integer.parseInt(playerParts.get(4));

    }

    public Player(String uniqueID) {
        this.uniqueID = uniqueID;
    }


    public String getUniqueID() {
        return uniqueID;
    }
    public String getPass() {
        return pass;
    }
    public int getRounds() {
        return rounds;
    }
    public int getWordsFound() {
        return wordsFound;
    }
    public int getPoints() {
        return points;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }
    public void setPass(String password) {
        this.pass = password;
    }
    public void setRounds(int rounds) {
        this.rounds = rounds;
    }
    public void setWordsFound(int wordsFound) {
        this.wordsFound = wordsFound;
    }
    public void setPoints(int points) {
        this.points = points;
    }


    @Override
    public String toString() {
        return (uniqueID + "," + pass + "," + rounds + "," + wordsFound + "," + points);
    }

    public void addRounds(int rounds)
    {
        this.rounds = this.rounds + rounds;
    }
    public void addWords(int words)
    {
        wordsFound = wordsFound + words;
    }
    public void addPoints(int points)
    {
        this.points= this.points + points;}


}
