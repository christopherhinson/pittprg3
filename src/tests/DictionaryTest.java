// CS 401
// Assignment 2 DictionaryTest program
// Simple demonstration of using the Dictionary class to retrieve
// random words and to test if a word is in the Dictionary or not

import java.io.*;
import java.util.*;
public class DictionaryTest
{
	public static void main(String [] args)
	{
		// Make WordServer object using dictionary file
		Dictionary D = new Dictionary("C:\\Users\\perso\\OneDrive\\Desktop\\pittprg2\\src\\dictionary.txt");
		
		// Get some words and show them.  Clearly you will do more
		// with the words than is shown here.
		System.out.println("Here are some words with at least 8 chars");
		for (int i = 1; i <= 10; i++)
		{
			String word = D.randWord(8);
			System.out.println("Next word is " + word);
		}
		System.out.println();
		
		// Test some words
		String word;
		word = "weasel";		testWord(D, word);
		word = "bogus";			testWord(D, word);
		word = "inconceivable"; testWord(D, word);
		word = "weasely";		testWord(D, word);
		word = "weaselly";	 	testWord(D, word);
		word = "sportsman";		testWord(D, word);
		
		word = "sportsmanlike"; testWord(D, word);
		D.addWord(word);
		testWord(D, word);
		
		word = "bodacious"; 	testWord(D, word);
		D.addWord(word);
		testWord(D, word);		
	}
	
	public static void testWord(Dictionary DD, String ww)
	{
		if (DD.contains(ww))
			System.out.println("Word " + ww + " is found");
		else
			System.out.println("Word " + ww + " is NOT found");
	}
}