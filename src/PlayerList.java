import javax.print.DocFlavor;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class PlayerList
{
    //location of the playerlist
    private String filePath;

    //this is the core of this file, the array that holds all players and their stats
    public ArrayList<Player> players = new ArrayList<Player>();

    //no-arg for keeping trtack of session players
    public PlayerList()
    {}

    //constructor that reads in all the player as a line string and turns them into player objects, then adds them to the array
    public PlayerList(String filePath) throws java.io.FileNotFoundException
    {
        File file = new File(filePath);
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNextLine())
        {
            Player nextPlayer = new Player(fileScanner.nextLine(), 1);
            players.add(nextPlayer);
        }
    }

    @Override
    public String toString()
    {

        StringBuilder contents = new StringBuilder();

        for (int index = 0; index<players.size();index++)
        {
            contents.append(players.get(index).toString() + "\n");
        }

        return contents.toString();
    }


    // containsId will return true if the Player's id is within the PlayerList and
    // false otherwise.  This will be useful in your main program.
    public Boolean containsId(String id)
    {
        for (int i = 0;i<players.size();i++)
        {
            if (players.get(i).getUniqueID().equals(id))
            {
                return true;
            }
        }

        return false;

    }

    public Boolean addPlayer(Player player)
    {
        try {
            players.add(player);
            return true;
        }
        catch (Exception e)
        {
            return Boolean.FALSE;
        }
    }

    public Player authenticate(Player player)
    {
        for(int i=0;i<players.size();i++)
        {
            if(player.getUniqueID().equals(players.get(i).getUniqueID())&&player.getPass().equals(players.get(i).getPass()))
            {
                return players.get(i);
            }
        }
        return null;
    }

    public void saveList(String name) throws IOException
    {
        File file = new File(name);
        file.delete();
        file.createNewFile();
        FileWriter fileWriter = new FileWriter(file);
        PrintWriter printWriter = new PrintWriter(fileWriter);
        for (int i = 0; i < players.size(); i++)
        {
            printWriter.print(players.get(i).getUniqueID() + ",");
            printWriter.print(players.get(i).getPass() + ",");
            printWriter.print(players.get(i).getRounds() + ",");
            printWriter.print(players.get(i).getWordsFound() + ",");
            printWriter.print(players.get(i).getPoints());
            printWriter.println();

        }
        printWriter.close();
    }
}
