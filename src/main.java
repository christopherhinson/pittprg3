import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

//bleh
public class main {
    public static void main (String[] args) throws IOException
    {

        Scanner k = new Scanner(System.in);

        System.out.println("How many people will be playing?");
        int numOfPlayers = k.nextInt();

        PlayerList sessionPlayers = new PlayerList();

        for(int a = 0;a<numOfPlayers;a++)
        {
            WordFinder wf = new WordFinder("C:\\Users\\perso\\OneDrive\\Desktop\\pittprg3\\src\\dictionary.txt");

            Boolean returningUser = Boolean.FALSE;
            Boolean running = Boolean.TRUE;

            System.out.println(
                    "Welcome to Anagrams, Here's how to play: \n" +
                            "    You will see a randomly selected word and in 60 seconds \n" +
                            "    you must find as many dictionary-valid words using the \n" +
                            "    characters of the random word as possible. You do not \n" +
                            "    have to use all the letters, but your score for a word \n" +
                            "    equals the number of characters it contains, so longer \n" +
                            "    is better ");

            System.out.println("would you like to play?  (Y/n)");

            //setting up our variables for the session
            int sessionScore = 0;
            int rounds = 0;
            int wordsFound = 0;


            PlayerList players = new PlayerList("C:\\Users\\perso\\OneDrive\\Desktop\\pittprg3\\src\\players.txt");
            Player player = null;

            //main game loop
            while (running) {
                String userResponse = k.next();
                if (userResponse.equals("Y") || userResponse.equals("y")) {

///////////////////loading previous stats///////////////////////////////////////////////////////////////////////////////

                    System.out.println("Are you a retuning user? (Y/n)");
                    userResponse = k.next();


                    if (userResponse.equals("Y") || userResponse.equals("y")) {
                        Boolean playerFound = Boolean.FALSE;
                        while (!playerFound) {
                            System.out.println("What is your userID?");
                            String userIDinputted = k.next();

                            for (int i = 0; i < players.players.size(); i++) {
                                if (userIDinputted.equals(players.players.get(i).getUniqueID())) {
                                    System.out.println("welcome back " + userIDinputted + ". What is your password");

                                    int attempts = 0;
                                    String passwordAttempt;
                                    Boolean validated = Boolean.FALSE;

                                    while (attempts < 3 && !validated) {
                                        passwordAttempt = k.next();
                                        if (players.players.get(i).getPass().equals(passwordAttempt)) {
                                            playerFound = Boolean.TRUE;
                                            validated = Boolean.TRUE;
                                            player = players.players.get(i);
                                            returningUser = Boolean.TRUE;
                                            System.out.println("Let's play " + player.getUniqueID());
                                        } else {
                                            attempts++;
                                            System.out.println("Sorry, that is not your password, try again. You have " + (3 - attempts) + " attempts left");
                                        }
                                    }
                                }
                            }
                            if (!playerFound) {
                                player = new Player();
                                System.out.println("Your userID has not been recognized, please create an account");
                                System.out.println("Enter an ID");
                                player.setUniqueID(k.next());
                                System.out.println("Ok, please enter a password");
                                player.setPass(k.next());
                                returningUser = Boolean.FALSE;
                                playerFound = Boolean.TRUE;
                            }
                        }
                    } else {
                        player = new Player();
                        System.out.println("Ok, welcome new user, please enter an id");
                        //TODO check if id is already in database
                        player.setUniqueID(k.next());
                        System.out.println("Ok, please enter a password");
                        player.setPass(k.next());
                        returningUser = Boolean.FALSE;

                    }


////////////////beginning of gameplay//////////////////////////////////////////////////////////////////////////////////

                    //setting up our variables for the round
                    ArrayList<String> userGoodResponses = new ArrayList<String>();

                    MyTimer t = new MyTimer();

                    int score = 0;


                    //set timer time
                    t.set(60000);

                    //get random word
                    wf.nextWord(7);

                    //get player ready
                    System.out.println("Word is : " + "\u001b[31m" + wf.showWord() + "\u001b[0m");
                    System.out.println("Timing will start when you enter your first word");

                    //start timer
                    t.start();

                    //main game code
                    while (t.check()) {
                        //get user's guess
                        String userGuess = k.next();

                        //determine if valid word
                        Boolean valid = wf.goodWord(userGuess);

                        //make sure timer hasn't expired before giving points
                        if (t.check()) {
                            //if valid word and not already used, give point
                            if (valid && !userGoodResponses.contains(userGuess)) {
                                userGoodResponses.add(userGuess);
                                System.out.println("That's a valid answer you have received " + userGuess.length() + " points");
                                score = score + userGuess.length();
                            }
                            //if valid word but previously used, do not give points and tell the user
                            else if (valid && userGoodResponses.contains(userGuess)) {
                                System.out.println("Thats a valid response, but you've already used that word.  " +
                                        "No points have been awarded");
                            }
                            //if not a valid word, tell the user
                            else {
                                System.out.println("Sorry, that's not a valid response");
                            }
                        }
                        //tell user when their time has elapsed
                        else
                            System.out.println("Sorry, time is up!");
                    }

                    //print round stats
                    System.out.println("You have received " + "\u001b[36m" + score + "\u001b[0m" +
                            " points after playing the following " + "\u001b[36m" + userGoodResponses.size() + "\u001b[0m"
                            + " words: ");
                    for (int i = 0; i < userGoodResponses.size(); i++) {
                        System.out.println(userGoodResponses.get(i));
                    }

                    //add to our session variables
                    sessionScore = sessionScore + score;
                    rounds++;
                    wordsFound = wordsFound + userGoodResponses.size();


                    //ask to start another round or not
                    System.out.println("\n Would you like to play again? (Y/n)");


                } else {

                    //insult the user if they're chosen to exit without playing
                    if (rounds == 0) {
                        System.out.println("You didn't even try, now that's just sad :(");
                        System.out.println("See you next time i guess :/");
                    } else {
                        //if the user has played >0 rounds, print their session stats
                        System.out.println("You played " + rounds + " rounds");
                        System.out.println("Finding " + wordsFound + " words");
                        System.out.println("Earning a total of " + sessionScore + " points");
                        System.out.println("That works out to be an average of " + (double) (wordsFound / rounds) + " words " +
                                "found per round; for an average points per round of " + (double) (sessionScore / rounds));

                        if (returningUser) {
                            player.addRounds(rounds);
                            player.addPoints(sessionScore);
                            player.addWords(wordsFound);

                            System.out.println("\n\n Over your lifetime: ");
                            System.out.println("You have played " + player.getRounds() + " rounds");
                            System.out.println("Finding " + player.getWordsFound() + " words");
                            System.out.println("Earning a total of " + player.getPoints() + " points");
                            System.out.println("That works out to be an average of " + (double) (player.getWordsFound() / player.getRounds()) + " words " +
                                    "found per round; for an average points per round of " + (double) (player.getPoints() / player.getRounds()));
                            System.out.println("Your stats have been automatically saved for the future");
                            sessionPlayers.addPlayer(player);
                        } else {
                            System.out.println("Would you like to save these stats for the future? (Y/n)");
                            userResponse = k.next();
                            if (userResponse.equals("Y") || userResponse.equals("y")) {
                                player.addRounds(rounds);
                                player.addPoints(sessionScore);
                                player.addWords(wordsFound);
                                players.addPlayer(player);
                                sessionPlayers.addPlayer(player);
                            }
                        }


/////////////////////Writing user's lifetime details back to their file/////////////////////////////////////////////////

                        players.saveList("C:\\Users\\perso\\OneDrive\\Desktop\\pittprg3\\src\\players.txt");


                        System.out.println("See you next time!");
                    }
                    running = Boolean.FALSE;
                }
            }
        }

        System.out.println("Thank you for playing. Here were your stats");
        System.out.println(sessionPlayers.toString());

    }
}

